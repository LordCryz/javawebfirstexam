package com.softtek.academy.javaweb.dao;

import com.softtek.academy.javaweb.beans.ToDo;
import com.softtek.academy.javaweb.service.ToDoService;

import java.util.*;
import java.util.stream.Collectors;

public class ToDoDao {

	public static int insertActivity(String name) {
		if (!name.isEmpty())
			return ToDoService.insertNewActivityService(name);
		return 0;
	}

	public static int updateActivity(int idActivity) {
		return ToDoService.updateActivity(idActivity);
	}

	public static List<ToDo> getAllActivities() {
		List<ToDo> todoList = ToDoService.getToDoActivities();
		List<ToDo> todoListFilter = todoList.stream().filter(todoItem -> todoItem.getIsDone().booleanValue() == false)
				.collect(Collectors.toList());

		return todoListFilter;

	}

	public static List<ToDo> getDoneActivities() {
		List<ToDo> todoList = ToDoService.getToDoActivities();
		List<ToDo> todoListFilter = todoList.stream().filter(todoItem -> todoItem.getIsDone().booleanValue())
				.collect(Collectors.toList());

		return todoListFilter;

	}

}
