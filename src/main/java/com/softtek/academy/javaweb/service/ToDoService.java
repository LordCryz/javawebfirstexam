package com.softtek.academy.javaweb.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.softtek.academy.javaweb.beans.ToDo;
import com.softtek.academy.javaweb.environment.ConnectionDB;

public class ToDoService {

	public static int insertNewActivityService(String activityName) {

		try {
			Connection con = ConnectionDB.getConnection();
			PreparedStatement ps = con.prepareStatement("INSERT INTO TO_DO_LIST(LIST) VALUES(?) ");
			ps.setString(1, activityName);
			int rs = ps.executeUpdate();
			return rs;

		} catch (Exception e) {
			System.out.println(e);
			return 0;
		}

	}

	public static int updateActivity(int id) {

		try {
			Connection con = ConnectionDB.getConnection();
			PreparedStatement ps = con.prepareStatement("UPDATE TO_DO_LIST set IS_DONE = 1 WHERE ID = ?");
			ps.setInt(1, id);
			int rs = ps.executeUpdate();
			return rs;

		} catch (Exception e) {
			System.out.println(e);
			return 0;
		}
	}

	public static List<ToDo> getToDoActivities() {
		List<ToDo> todoList = new ArrayList<ToDo>();

		try {
			Connection con = ConnectionDB.getConnection();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM TO_DO_LIST");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				ToDo todoItem = new ToDo();
				todoItem.setId(rs.getInt("ID"));
				todoItem.setList(rs.getString("LIST"));
				todoItem.setIsDone(rs.getBoolean("IS_DONE"));
				todoList.add(todoItem);
			}
			return todoList;

		} catch (Exception e) {
			System.out.println(e);
			return todoList;
		}
	}

}
