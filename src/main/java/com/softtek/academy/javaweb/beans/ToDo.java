package com.softtek.academy.javaweb.beans;

public class ToDo {
	private int id;
	private String list;
	private Boolean isDone;

	public ToDo(int id, String list, Boolean isDone) {
		super();
		this.id = id;
		this.list = list;
		this.isDone = isDone;
	}

	public ToDo() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getList() {
		return list;
	}

	public void setList(String list) {
		this.list = list;
	}

	public Boolean getIsDone() {
		return isDone;
	}

	public void setIsDone(Boolean isDone) {
		this.isDone = isDone;
	}

}
