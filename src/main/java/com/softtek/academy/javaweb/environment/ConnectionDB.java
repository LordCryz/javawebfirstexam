package com.softtek.academy.javaweb.environment;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionDB {

	public static Connection getConnection() {
		final String JDBC_DRIVE = "com.mysql.jdbc.Driver";
		final String DB_URL = "jdbc:mysql://localhost:3306/jspapplication"
				+ "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		final String USER = "root";
		final String PASS = "1234";

		Connection con = null;

		try {
			Class.forName(JDBC_DRIVE);
			con = DriverManager.getConnection(DB_URL, USER, PASS);

		} catch (Exception e) {
			System.out.println(e);
		}

		return con;

	}

}
