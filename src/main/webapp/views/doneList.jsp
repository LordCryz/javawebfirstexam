<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Done activities list</title>
</head>
<body>

	<table style="width: 100%">
		<tr>
			<td>ID</td>
			<td>Activity</td>
		</tr>
		<c:forEach items="${todoList}" var="item">
			<tr>
				<td>${item.getId()}</td>
				<td>${item.getList()}</td>

			</tr>

		</c:forEach>


	</table>

</body>
</html>