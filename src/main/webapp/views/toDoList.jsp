<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>TO DO List</title>
</head>
<body>

	<table style="width: 100%">
		<tr>
			<td>ID</td>
			<td>Activity</td>
			<td>Select activity</td>
		</tr>
		<c:forEach items="${todoList}" var="item">
			<tr>
				<td>${item.getId()}</td>
				<td>${item.getList()}</td>
				<td>
					<form action="./ToDoUpdateControllerPage" method="get">
						<button type="submit">Done</button>
						<input type="hidden" name="id" value="${item.getId()}">
					</form>

				</td>
			</tr>

		</c:forEach>


	</table>


</body>
</html>